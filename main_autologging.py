from __future__ import print_function

from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split

import mlflow
import mlflow.sklearn

if __name__ == "__main__":
    mlflow.set_experiment("/lr_testing_autologging")
    mlflow.sklearn.autolog()

    with mlflow.start_run(run_name='base_run'):
        # параметры логит-регрессии
        C = .02
        tol = .00002

        X, y = make_classification(n_features=5, n_redundant=0, n_informative=2, n_samples=150, random_state=42)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
        # пайплайн: нормирование + логит-регрессия
        model = Pipeline(steps=[
            ('preprocessor', MinMaxScaler()),
            ('estimator', LogisticRegression()),
        ])
        # считываем dict c лучшими параметрами, подобранными Оптуной
        with open('best_params.txt', 'r') as f:
            best_params = eval(f.read())
        model.set_params(**best_params)
        # обучаем модель, считаем метрику
        model.fit(X_train, y_train)
        score = model.score(X_test, y_test)

        print("Model saved in run %s" % mlflow.active_run().info.run_uuid)