# coding=utf-8
import optuna
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score

def objective(trial):

    X, y = make_classification(n_features=5, n_redundant=0, n_informative=2, n_samples=150, random_state=42)
    X_train, _, y_train, _ = train_test_split(X, y, test_size=0.2, random_state=42)

    # пайплайн: нормирование + логит-регрессия
    model = Pipeline(steps=[
        ('preprocessor', MinMaxScaler()),
        ('estimator', LogisticRegression()),
    ])
    # определяем возможные диапазоны параметров
    param = {
        'estimator__C': trial.suggest_uniform('estimator__C', 0.01, 5),
        'estimator__tol': trial.suggest_uniform('estimator__tol', 0.0001, 0.001),
    }
    model.set_params(**param)
    # считаем метрику на кросс-валидации на трейне, которую будем максимизировать
    train_cv_accuracy = np.mean(cross_val_score(model, X_train, y_train, cv=3, scoring='accuracy'))

    return train_cv_accuracy
# создаем study и запускаем перебор
study = optuna.create_study(direction='maximize')
study.optimize(objective, n_trials=100)
# записываем оптимальный набор параметров, чтобы потом его прокидывать в пайплайн
with open('best_params.txt', 'w') as f:
    f.write(str(study.best_params))

print('Number of finished trials:', len(study.trials))
print('Best trial:', study.best_trial.params)